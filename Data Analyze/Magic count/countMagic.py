import magic as mg
import os
import sys
import numpy as np

def count(directory,fileName):
    try:
        fileTypes=[]
        for subdir, dirs, files in os.walk(directory):
            for file in files:
                path=os.path.join(subdir, file)
                if not('hts-log.txt' in path or 'hts-cache' in path or 'hts-in_prog' in path):
                    fileTypes.append(mg.from_file(path).split(",")[0])
        
        uniqueTypes=np.unique(fileTypes,return_counts=True)
        toFile=np.column_stack((uniqueTypes[0],uniqueTypes[1]))
        out=str(fileName)
        with open(out,'w') as f:    
            np.savetxt(f,toFile ,fmt='%s', delimiter=',')
    except Exception,e:
        print "Error 2: ",str(e)
        
def readFile(directory):
    dirs=[]
    names=[]
    try:    
        with open(directory,'r') as f:
            content=f.read().splitlines()
            for line in content:
                temp=line.split(";")
                dirs.append(temp[0])
                names.append(temp[1])
    except Exception,e:
        print "Error 1: ",str(e)
    return (dirs,names)
    
if __name__ == '__main__':
    dirs,names=readFile(sys.argv[1])
    for i in range(0,len(dirs)):    
        count(dirs[i],names[i])
