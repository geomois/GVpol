import os
import sys
from bs4 import BeautifulSoup as bs
from bs4 import Comment
import beautyUtils

def fileWalker(directory):
    filesToScrape=[]
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            this=os.path.join(subdir, f)
            if f.endswith(".html") and this!=str(directory+'/index.html'):
#                count+=1
                filesToScrape.append(this)
    print 'html count ',len(filesToScrape)
    return filesToScrape

def media(url,tags,comment,site):
        files=[]
        tagLinks=[]
        for im in tags:
            tempDict={}
            tempDict['domain']=site[len(site)-2]+'.'+site[len(site)-1]
             
            tempDict['url']=url
            #prepei na tsekarw an einai kanoniko 'h relative 
            #                 [f.split('/')[x] for x in range(1,len(f.split('/')))]
            try:
                src=im['src']

                tempDict['filename']=os.path.basename(src)
                tempDict['filetype']=os.path.splitext(src)[1]
                tempDict['media_path']=src
            except:
#                tempDict['path']=None
                tempDict['filename']=''
                tempDict['filetype']=''
                tempDict['media_path']=''
                
            tempDict['download_date']=str(comment[10])+"/"+str(comment[11])+"/"+str(comment[12])
            tempDict['media_title']=tempDict['filename'].split('.')[0]
            t=url.split('/')
            tempDict['media_link']='/'.join(t[:len(t)-1])+'/'+tempDict['media_path']
            if tempDict['media_link'] not in tagLinks:
                files.append(tempDict.copy())
                tagLinks.append(tempDict['media_link'])

            #            imgFiles=[]
#            for im in imgs:
#                imgDict={}
#                imgDict['domain']=site[len(site)-2]+'.'+site[len(site)-1]
#                
#                imgDict['url']=url
#                #prepei na tsekarw an einai kanoniko 'h relative 
##                 [f.split('/')[x] for x in range(1,len(f.split('/')))]
#                imgDict['path']=im['src']
#                imgDict['filename']=os.path.basename(im['src'])
#                imgDict['download_date']=str(comment[10])+"/"+str(comment[11])+"/"+str(comment[12])
#                imgDict['filetype']=os.path.splitext(im['src'])[1]
#                imgDict['media_title']=imgDict['filename'].split('.')[0]
#                imgDict['media_path']=im['src']
#                t=url.split('/')
#                imgDict['media_link']='/'.join(t[:len(t)-1])+'/'+imgDict['path']
#                 imgDict['category'] no categories in pkk
#                imgFiles.append(tempDict.copy())
        
        return files
        

def extract(filePaths,siteName):
#    pkk
#Always needed: domain,?hostname?,url,path,filename,download date, filetype,title,content,relative path
#Depends on content we need: raw(html),out links,media links,category(taken from the url probably),thread_id(forum),publish date
    extractedInfo=[]
    for files in filePaths:
#        parsable: 'url','mainArticle'(dict),'audio'(dict),'video'(dict),'audio'(dict),
        parsable={}
        with open(files,'r') as f:
            doc=f.read().splitlines()
#            Get rid of new lines
            soup= bs(''.join(doc))
            
            title=soup.find("title")
            if title is not None:
                if title.text == "Page has moved":
                    continue            
            
#           httrack puts a comment-> url/timestamp of the form:
#           Mirrored from www.pkkonline.com/en/index.php?sys=article&artID=123 by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 02 Jun 2016 08:41:05 GMT
            comment=soup.find_all(string=lambda text:isinstance(text,Comment))
            for com in comment:
                if com.startswith(" Mirrored"):
                    comment=com
                    break
                
            if comment is not None and 'Mirrored from' in comment: ##### left from changes, needs cleaning
                comment=comment.split(' ')

                url=comment[3]
                site=url.split('/')[0].split('.')
    #            Find media files first, prepare for parsing
    #            images            
                imgs= soup.find_all('img')
                imgFiles=media(url,imgs,comment,site)
    #            video  
                videos=soup.find_all('video')
                videoFiles=media(url,videos,comment,site)               
    #            audio
                audios=soup.find_all('audio')
                audioFiles=media(url,audios,comment,site)
                
    #            links
                tempLinks=soup.find_all('a',href=True)
                linksList=[]
                for link in tempLinks:
                    linkDict={}
                    t=link['href']
####################for  noState############################
                    if "http" not in t:#noState ONLY, posts are in index.html files in each folder
                        split=t.split('/')
                        t='/'.join(url.split('/')[:len(url.split('/'))-1])+'/'+'/'.join(split[:len(split)-1])#nostate ONLY,  vgazw to index.html apo thn dieu8ynsh (to xei pros8esei to HT) INTERNAL LINKS
                    linkDict['url']=t
#                    else:
#                        linkDict['url']=t
                    try:                
                        linkDict['title']=link['title']
                    except:
                        linkDict['title']=None
                        
                    linksList.append(linkDict.copy())
    
    #            Article
                section=soup.find('section', { "id" : "content" }) #noState ONLY
                articleDict={}
                if section is not None:
                    article=section.find("article", {"class" : "post"})#noState ONLY
                    if article is not None:            
                        articleDict['text']=article.text
                        tempLinks=article.find_all('a',href=True)       
                        articleLinks=[]
                        for link in tempLinks:
                            linkDict={}
                            t=link['href']
####################for  noState############################
                            if "http" not in t:#noState ONLY, posts are in index.html files in each folder
                                split=t.split('/')
                                t='/'.join(url.split('/')[:len(url.split('/'))-1])+'/'+'/'.join(split[:len(split)-1])#nostate ONLY,  vgazw to index.html apo thn dieu8ynsh (to xei pros8esei to HT) INTERNAL LINKS
                            linkDict['url']=t
        #                    else:
        #                        linkDict['url']=t
                            try:                
                                linkDict['title']=link['title']
                            except:
                                linkDict['title']=None

                            articleLinks.append(linkDict.copy())
                        articleDict['links']=articleLinks
                
    #            toParsable
    #            html file
                parsable['domain']=site[len(site)-2]+'.'+site[len(site)-1]
                parsable['url']=url
    #            check what path we need exactly, the one to the local file or online
                parsable['path']='/'.join(url.split('/')[1:len(url)])
                parsable['filename']=os.path.basename(files)
                parsable['download_date']=str(comment[10])+"/"+str(comment[11])+"/"+str(comment[12])
                parsable['filetype']=os.path.splitext(os.path.basename(files))[1]
    
                parsable['mainArticle']=articleDict.copy()
                parsable['url']=url
                parsable['audio']=audioFiles
                parsable['video']=videoFiles
                parsable['image']=imgFiles  
                parsable['links']=linksList
                extractedInfo.append(parsable.copy())
#                print "done ",len(extractedInfo)
#            soup.prettify()
#            for external links
    overInfo={}
    print "done ",len(extractedInfo)
    overInfo[siteName]=extractedInfo
    beautyUtils.dumpToJson(overInfo)

def readFile(directory):
    dirNames=[]
    siteNames=[]
    with open(directory,'r') as f:
        content=f.read().splitlines()
        for line in content:
            dirNames.append(line.split(';')[0])
            siteNames.append(line.split(';')[1])
    return dirNames,siteNames
    
if __name__ == '__main__':
    dirNames,siteNames=readFile("/home/iam/Desktop/extractB/paths")
#    dirNames,siteNames=readFile(sys.argv[1])
    for i in range(0,len(dirNames)):
        files=fileWalker(dirNames[i])
        extract(files,siteNames[i])    

        
#files=fileWalker("/home/iam/Desktop/extractB/pkk/www.pkkonline.com/")
#extract(files)