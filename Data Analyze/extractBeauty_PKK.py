import os
#import sys
#import hashlib
from bs4 import BeautifulSoup as bs
from bs4 import Comment
import beautyUtils
import re

def fileWalker(directory):
    filesToScrape=[]
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            if f.endswith(".html"):
#                count+=1
                filesToScrape.append(os.path.join(subdir, f))

    return filesToScrape

def media(url,tags,comment,site):
        files=[]
        tagLinks=[]
        for im in tags:
            tempDict={}
            tempDict['domain']=site[len(site)-2]+'.'+site[len(site)-1]
            tempDict['use_case']='national_separatists'
#            tempDict['url']=url
            #prepei na tsekarw an einai kanoniko 'h relative
            #                 [f.split('/')[x] for x in range(1,len(f.split('/')))]
            try:
                src=im['src']
#                tempDict['path']=src
                if '&' in src:                
                    try:                
                        real=re.search('(?<=\=)[a-zA-Z0-9\/.]*',src).group()
                        src=real
                    except:
                        src=im['src']
                    
                tempDict['filename']=os.path.basename(src)
                tempDict['filetype']=os.path.splitext(src)[1]
                tempDict['media_path']=src
            except:
#                print src, " ", im['src'],
                tempDict['filename']=''
                tempDict['filetype']=''
                tempDict['media_path']=''
            
            tempDict['download_date']=str(comment[10])+"/"+str(comment[11])+"/"+str(comment[12])
            tempDict['media_title']=tempDict['filename'].split('.')[0]
            t=url.split('/')
            tempDict['media_link']='/'.join(t[:len(t)-1])+'/'+tempDict['media_path']
            if tempDict['media_link'] not in tagLinks:
                files.append(tempDict.copy())
                tagLinks.append(tempDict['media_link'])
            
        return files

def extract(filePaths,siteName):
#    pkk
#Always needed: domain,?hostname?,url,path,filename,download date, filetype,title,content,relative path
#Depends on content we need: raw(html),out links,media links,category(taken from the url probably),thread_id(forum),publish date
    extractedInfo=[]
    for files in filePaths:
#        parsable: 'url','mainArticle'(dict),'audio'(dict),'video'(dict),'audio'(dict),
        parsable={}
        with open(files,'r') as f:
            doc=f.read().splitlines()
#            Get rid of new lines
            soup= bs(''.join(doc))
#           httrack puts a comment-> url/timestamp of the form:
#           Mirrored from www.pkkonline.com/en/index.php?sys=article&artID=123 by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 02 Jun 2016 08:41:05 GMT
            comment=soup.find(string=lambda text:isinstance(text,Comment))
            if comment is not None and 'Created by HTTrack' not in comment and 'Mirrored from' in comment: ##### PAIZEI NA NAI MEGALH PAPARIA
                comment=comment.split(' ')
                url=comment[3]
                site=url.split('/')[0].split('.')

    #            Find media files first, prepare for parsing
    #            images            
                imgs= soup.find_all('img')
                imgFiles=media(url,imgs,comment,site)
    #            video  
                videos=soup.find_all('video')
                videoFiles=media(url,videos,comment,site)               
    #            audio
                audios=soup.find_all('audio')
                audioFiles=media(url,audios,comment,site)

       #            links
                tempLinks=soup.find_all('a',href=True)
                linksList=[]
                for link in tempLinks:
                    linkDict={}
                    t=link['href']
####################for PKK ONLY############################
                    if '?sys' in t:
                        t='/'.join(url.split('/')[:len(url.split('/'))-1])+'/'+''.join(t.split('?')[1:])
                        linkDict['url']=t
                    else:
                        linkDict['url']='/'.join(url.split('/')[:len(url.split('/'))-1])+'/'

                    try:                
                        linkDict['title']=link['title']
                    except:
                        linkDict['title']=''
                        
                    linksList.append(linkDict.copy()) 


    #            Article
                
                div=soup.find(id="artdiv")
                articleDict={}
                if div is not None:            
                    articleDict['text']=div.text
                    tempLinks=div.find_all('a',href=True)       
                    articleLinks=[]
                    for link in tempLinks:
                        linkDict={}
####################for PKK ONLY############################
                        t=link['href']
                        if '?sys' in t:
                            t='/'.join(url.split('/')[:len(url.split('/'))-1])+'/?'+''.join(t.split('?')[1:])
                            linkDict['url']=t
                        else:
                            linkDict['url']='/'.join(url.split('/')[:len(url.split('/'))-1])+'/'                     
        #                        linkDict['url']=link['href']
                        try:                
                            linkDict['title']=link['title']
                        except:
                            linkDict['title']=''
#                        try:
#                            linkDict['content']=link.contents[0]
#                        except:
#                            linkDict['content']=None
                        articleLinks.append(linkDict.copy())
                    articleDict['links']=articleLinks
                    
    #            toParsable
    #            html file
                parsable['domain']=site[len(site)-2]+'.'+site[len(site)-1]
                parsable['url']=url
    #            check what path we need exactly, the one to the local file or online
                parsable['path']='/'.join(url.split('/')[1:len(url)])
                parsable['filename']=os.path.basename(files)
                parsable['download_date']=str(comment[10])+"/"+str(comment[11])+"/"+str(comment[12])
                parsable['filetype']=os.path.splitext(os.path.basename(files))[1]
    
                parsable['mainArticle']=articleDict.copy()
                parsable['url']=url
                parsable['audio']=audioFiles
                parsable['video']=videoFiles
                parsable['image']=imgFiles  
                parsable['links']=linksList
                extractedInfo.append(parsable.copy())
#                print "done ",len(extractedInfo)
        
        print "done ",len(extractedInfo)
#            soup.prettify()
#            for external links
    
    overInfo={}
    overInfo[siteName]=extractedInfo
    beautyUtils.dumpToJson(overInfo)

def readFile(directory):
    dirNames=[]
    siteNames=[]
    with open(directory,'r') as f:
        content=f.read().splitlines()
        for line in content:
            dirNames.append(line.split(';')[0])
            siteNames.append(line.split(';')[1])
    return dirNames,siteNames

if __name__ == '__main__':
    dirNames,siteNames=readFile("/home/iam/Desktop/extractB/paths")
#    dirNames,siteNames=readFile(sys.argv[1])
    for i in range(0,len(dirNames)):
        files=fileWalker(dirNames[i])
        extract(files,siteNames[i])

