import tweepy
import csv
import sys


#Twitter API credentials
consumer_key = ''
consumer_secret = ''
access_key = ''
access_secret = ''



def get_all_tweets(screen_name):
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_key, access_secret)
        api = tweepy.API(auth)

        alltweets = []

        new_tweets = api.user_timeline(screen_name = screen_name,count=200)
        alltweets.extend(new_tweets)
        oldest = alltweets[-1].id - 1

        while len(new_tweets) > 0:
                print "getting tweets before %s" % (oldest)
                new_tweets = api.user_timeline(screen_name = screen_name,count=200,max_id=oldest)
                alltweets.extend(new_tweets)
                oldest = alltweets[-1].id - 1

                print "...%s tweets downloaded far" % (len(alltweets))

        outtweets = [[tweet.id_str, tweet.created_at,tweet.author.screen_name,tweet.retweet_count,tweet.in_reply_to_user_id_str,tweet.in_reply_to_screen_name, tweet.text.encode("utf-8"),tweet.place] for tweet in alltweets]        

        with open('%s_tweets.csv' % screen_name, 'wb') as f:
                writer = csv.writer(f)
                writer.writerow(["id_str", "created_at","author","retweet_count","in_reply_to_user_id_str","in_reply_to_screen_name", "text","place"])
                writer.writerows(outtweets)

        pass


if __name__ == '__main__':
        get_all_tweets(sys.argv[1])
