In twitterCrawler.py have been implemented all the other scripts of this folder in a more enchanced form.
In this readme I describe only the functionality of twitterCrawler.py

To run: python twitterCrawler.py [options][arguments]

Options:
-h(n): sql mode, all other sql-based functions are implemented under this flag. If "n" flag is used,
       no streams will start. If "n" is not present the streams will start after the asked job is done
    -ho: goes through current folder and analyzes .csv files extracts the "author" of a saved tweet
         adding him/her in a list of profiles that will be crawled (timelines)
    -hs: expecst argument: <path to file> . Like -ho but for a specific file
    -hc: expects argument: <hashtag text> . It downloads tweets with specific hashtag beggining a week ago,
         and saves them in a .csv file. 
         *This function was used in the first versions should be used after testing
    -ha: expects argument: <author name> . It downloads all the tweets of the user adding them to the database
-j: json mode, starts the streams and saves the tweets in json files
    -jc: expects argument: <hashtag text> . Similarly to -hc but no .csv file is written, only json 
         *This function was used in the first versions, should be used after testing