# -*- coding: utf-8 -*-
import tweepy
import csv
import sys
import datetime
import time
import json
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import psycopg2
import os
from dateutil import parser
from beautyUtils import dumpToJson

reload(sys)
sys.setdefaultencoding('utf8') # set encoding to use Arabic letters

# create table twitter (
# tweetId varchar(50) constraint idKey PRIMARY KEY,
# author varchar(50),
# retCount int,
# inRepId varchar(50),
# inRepName varchar(50),
# tweetText text,
# place text,
# crawled boolean,
# tweetDate timestamp);

consumer_key = ''
consumer_secret = ''
access_key = ''
access_secret = ''


def get_all_tweets(screen_name, auth):
    api = tweepy.API(auth)
    alltweets = []
    while True:
        try:
            new_tweets = api.user_timeline(screen_name=screen_name, count=200)
            alltweets.extend(new_tweets)
            oldest = alltweets[-1].id - 1
            break
        except Exception,e:
            print "Twitter error:", str(e)
            bg=api.rate_limit_status()                
            if bg['resources']['statuses']['/statuses/user_timeline']['remaining'] <5:
                print("Waiting the TwitterAPI timeout")
                time.sleep(60*15)
                continue
            else:
                return
        
    
    while len(new_tweets) > 0:
        try:
            print "getting tweets before %s" % (oldest)
            new_tweets = api.user_timeline(screen_name=screen_name, count=200, max_id=oldest)
            alltweets.extend(new_tweets)
            oldest = alltweets[-1].id - 1
            print "...%s tweets downloaded far" % (len(alltweets))
        except Exception, e:
            print "Twitter error:", str(e)
            bg=api.rate_limit_status()                
            if bg['resources']['statuses']['/statuses/user_timeline']['remaining'] <5:
                print("Waiting the API timeout")
                time.sleep(60*15)

    outtweets = []
    for tweet in alltweets:
        temp = None
        if tweet.place is not None:
            temp = tweet.place.full_name

        outtweets.append([tweet.id_str, tweet.created_at, tweet.author.screen_name, tweet.retweet_count,
                          tweet.in_reply_to_user_id_str, tweet.in_reply_to_screen_name, tweet.text.encode("utf-8"),
                          temp])

    with open('%s_tweets.csv' % ('USER_' + str(screen_name)), 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(
            ["id_str", "created_at", "author", "retweet_count", "in_reply_to_user_id_str", "in_reply_to_screen_name",
             "text", "place"])
        writer.writerows(outtweets)

    return alltweets

#used in first versions, not implemented db connection, json implemented, not tested
def get_hashtags(hashtag, auth,jsonFlag=False):#fetches the tweets with the given hashtags from a week before due to twitter API restrictions
    api = tweepy.API(auth)
    cursor = tweepy.Cursor(api.search, q=hashtag).items()
    now = datetime.datetime.now().date().isoformat()
    now = ''.join(now.split('-'))[:8]
    outtweets = []
    count = 0
    while True:
        try:
            tweet = cursor.next()
            count += 1
            if count % 100 == 0:
                print 'Tweets so far: %d' % (count)
            if jsonFlag: # dump to json instead of csv
                dumpToJson(tweet,hashtag+now)
                continue
                        
            temp = None
            if tweet.place is not None:
                temp = tweet.place.full_name
            outtweets.extend([[tweet.id_str, tweet.created_at, tweet.author.screen_name, tweet.retweet_count,
                               tweet.in_reply_to_user_id_str, tweet.in_reply_to_screen_name, tweet.text.encode("utf-8"),
                               temp]])
        except tweepy.TweepError:
            time.sleep(60 * 15)
            continue
        except StopIteration:
            break
        
    if not jsonFlag:
        with open('#%s_tweets.csv' % hashtag, 'wb') as f:
            writer = csv.writer(f)
            writer.writerow(
                ["id_str", "created_at", "author", "retweet_count", "in_reply_to_user_id_str", "in_reply_to_screen_name",
                 "text", "place"])
            writer.writerows(outtweets)


class TwiListener(StreamListener):
    def __init__(self, auth,jsonFlag=False,query=None):
        self.auth = auth
        now = datetime.datetime.now().date().isoformat()
        self.now = ''.join(now.split('-'))[:8]
        if query is not None:        
            self.outfile = "stream_%s_%s.csv" % (query,self.now)
            with open(self.outfile, 'a') as f:
                writer = csv.writer(f)
                writer.writerow(["id_str", "created_at", "author", "retweet_count", "in_reply_to_user_id_str",
                             "in_reply_to_screen_name", "text", "place"])
        self.jsonFlag=jsonFlag
        self.count = 0
        if not jsonFlag:        
            try:
                self.soc = psycopg2.connect("dbname='' user='' host='' password=''")
            except:
                print "Error creating connection to Database"

    def on_data(self, data):
        try:
            if self.jsonFlag:#ommit adding to database, write to json file
                dumpToJson(data,self.now)
                self.count += 1
                if self.count % 100 == 0:
                    print 'Tweets so far: %d' % (self.count)
                return True
                
            with open(self.outfile, 'a') as f:
                writer = csv.writer(f)
                tweet = json.loads(data)
                

                try:
                    if tweet['place'] is not None:
                        tweet['place'] = tweet['place']['full_name']
                except:
                    tweet['place']=None

                inTo = [tweet['id_str'], tweet['created_at'], tweet['user']['screen_name'], tweet['retweet_count'],
                        tweet['in_reply_to_user_id_str'], tweet['in_reply_to_screen_name'], tweet['text'],
                        tweet['place']]
                outtweet = [unicode(s).encode('UTF-8') for s in inTo]
                writer.writerow(outtweet)
                self.count += 1
                if self.count % 100 == 0:
                    print 'Tweets so far: %d' % (self.count)
                self.analyzeCurrent(tweet)
            self.addToDatabase(tweet, crawled=False)
            return True

        except BaseException as e:
            print("Error on_data: %s" % str(e))
            time.sleep(5)
        return True

    def on_error(self, status):
        print(status)
        return True

    def analyzeCurrent(self, tweet=None, path=None):
        #    walks through the current files for users that used specific hashtags or
        #    checks whether we have crawled a user's timeline already and crawls it if not
        if tweet == None:
            userId = []
            if path is not None:#analyze one specific file
                with open(path, 'rb') as csvfile:
                    print "Reading file.."
                    lines = csv.reader(csvfile)
                    for row in lines:
                        if row[0] == 'id_str':
                            continue
                        if row[2] not in userId:
                            userId.append(row[2])
            else:#analyze all the csv files in our directory, not secure, the csv files in the directory should be created from this script
                for subdir, dirs, files in os.walk(os.curdir):
                    for f in files:
                        if f.endswith('.csv'):
                            with open(f, 'rb') as csvfile:
                                lines = csv.reader(csvfile)
                                for row in lines:
                                    if row[0] == 'id_str':
                                        continue
                                    if row[2] not in userId:
                                        userId.append(row[2])
            self.searchAndCrawl(userId)
        else:
            pass# useless, leftover from previous versions

    def searchAndCrawl(self, nameList):#search the database if a user's profile is already crawled, if not we crawl it
        if self.soc != 1:
            try:
                cursor = self.soc.cursor()
            except:
                print "Error creating connection to Database"
                return 0

        for i in range(0, len(nameList)):
            try:
                cursor.execute("""select crawled from twitter where author='%s' and crawled is True limit 1;""" % nameList[i])
                fetched = cursor.fetchall()
                if len(fetched) > 0:
                    continue
                else:
                    toAdd = get_all_tweets(nameList[i], self.auth)
                    try:
                        self.addToDatabase(toAdd, cursor, True)
                        self.markCrawled(nameList[i])#update database that a specific user is crawled IT CAN TAKE LONG IF CALLED WITH NO AUTHOR
                    except Exception, e:
                        print "Unexpected error:", str(e)

            except Exception, e:
                print "Unexpected error:", str(e)
                self.soc.rollback()

        cursor.close()

    def addToDatabase(self, tweets, cursor=None, crawled=None):# adding tweets to DB, (!)streams bring back different data types from User/profile crawling
        cFlag=False #if we call addToDatabase() from searchAndCrawl() we need the cursor NOT to close 
        if cursor is None:
            try:#setting up a new cursor for the transactions, if needed
                cFlag=True 
                cursor = self.soc.cursor() #this cursor will close when done
            except:
                return 0
                
        if crawled:#if not crawled, comming from streams
            for tweet in tweets:
                try:
                    temp = None

                    try:
                        if tweet.place is not None:
                            temp = tweet.place.full_name
                    except:
                        temp =None

                    cursor.execute(
                        """insert into twitter (tweetId,author,retCount,inRepId,inRepName,tweetText,place,crawled,tweetDate) values (%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                        (tweet.id_str, tweet.author.screen_name, tweet.retweet_count, tweet.in_reply_to_user_id_str,
                         tweet.in_reply_to_screen_name, tweet.text.encode("utf-8"), temp, crawled,
                         tweet.created_at))  # tweet.created_at
                    self.soc.commit()
                except:
                    self.soc.rollback()
        else:
            tweet = tweets
            try:
                cursor.execute(
                    """insert into twitter (tweetId,author,retCount,inRepId,inRepName,tweetText,place,crawled,tweetDate) values (%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                    (tweet['id_str'], tweet['user']['screen_name'], tweet['retweet_count'],
                     tweet['in_reply_to_user_id_str'], tweet['in_reply_to_screen_name'], tweet['text'], tweet['place'],
                     crawled, parser.parse(tweet['created_at'])))  # tweet['created_at']
                self.soc.commit()
            except:
                self.soc.rollback()
        if cFlag:
            cursor.close()

    def markCrawled(self,author=None):
        try:
                cursor = self.soc.cursor()
        except:
                return 0
                
        if author is None:
            try:
                cursor.execute("""UPDATE twitter SET crawled= (CASE WHEN author IN (SELECT author FROM twitter WHERE crawled = True) THEN True else False END);""")
                self.soc.commit()
            except:
                self.soc.rollback()
        else:
            try:
                cursor.execute("""UPDATE twitter SET crawled= True where author = '%s'""" % author)
                self.soc.commit()
            except:
                self.soc.rollback()            
    
if __name__ == '__main__':
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    #I/You can add a small snippet to read the query list from file
    #in beautyUtils.py there is one implemented, but it needs to be altered so it serves this script's needs
    ar1 = unicode('#اصدارات_الخلافة', encoding='utf-8')
    ar2 = unicode('#الدولة_الإسلامية', encoding='utf-8')
    queryList = ['#dieinyourrage', '#TwitterKurds', '#baqiyahfamily', '#isischan', ar1, ar2]
    if sys.argv[1].startswith('-h'):#hashtags
        if sys.argv[1] == '-ho':#analyze files in folder
            myListener = TwiListener(auth,False,None)
            myListener.analyzeCurrent()
            print 'Job done'
        if 'c' in sys.argv[1]:#download hashtags (from a week ago until now) and save in .csv | somewhat old
            for q in queryList:
                get_hashtags(q, auth)
            print 'Job done'
        if 's' in sys.argv[1]:# analyze specific file
            path = sys.argv[2]
            myListener = TwiListener(auth,False,None)
            myListener.analyzeCurrent(tweet=None, path=path)
            print 'Job done'
        if 'a' in sys.argv[1]:#download specific user's tweets
            myListener = TwiListener(auth,False,None)
            toAdd=get_all_tweets(sys.argv[2], auth)
            myListener.addToDatabase(toAdd, None, True)
            print 'Job done'
        if 'n' not in sys.argv[1]:#start streams
            myListener = TwiListener(auth,sys.argv[2])
            twitter_stream = Stream(auth, myListener)
            print 'Starting streams..'
            twitter_stream.filter(track=queryList, async=True)
    elif sys.argv[1].startswith('-j'):#skip DB, dump to json, ONLY STREAMS implemented
        if 'c' in sys.argv[1]:
            get_hashtags(sys.argv[2])
            print "Job done"
        else:
            myListener = TwiListener(auth,True,None)
            twitter_stream = Stream(auth, myListener)
            print 'Starting streams..'
            twitter_stream.filter(track=queryList, async=True)